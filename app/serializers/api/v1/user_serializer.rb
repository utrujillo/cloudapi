module Api
  module V1

    class UserSerializer < ActiveModel::Serializer
      attributes :id, :firstname, :lastname, :email, :birthDate
      has_many :addresses
    end

  end
end
