module Api
  module V1

    class AddressSerializer < ActiveModel::Serializer
      attributes :id, :street, :city, :country, :postalcode
      has_one :user
    end

  end
end
