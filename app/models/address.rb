class Address < ApplicationRecord
  # Assosiations
  belongs_to :user
  # Custom Enum Fieldss
  enum country: [ :ES, :UK, :DE, :US ]
end
