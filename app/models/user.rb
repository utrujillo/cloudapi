class User < ApplicationRecord
  # Validations
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/, message: "Bad email format" }
  validates :birthDate, format: { with: /^\d{4}-\d{2}-\d{2}$/, multiline: true }
  
  # Nested assosiations
  has_many :addresses, dependent: :destroy
  accepts_nested_attributes_for :addresses,
                                reject_if: :all_blank,
                                allow_destroy: true
end
