Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      get 'addresses/get_countries', to: 'addresses#get_countries'
      resources :users
      resources :addresses
    end
  end
  
end
